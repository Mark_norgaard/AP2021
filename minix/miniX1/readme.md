**Screenshot:**
<br>
![](miniX1_screen.png)

**Program link:**
<br>
[Click here!](https://celia_w.gitlab.io/AP2021/minix/miniX1/)

**Code link:**
<br>
[Click here!](https://gitlab.com/celia_w/AP2021/-/blob/master/miniX1/pikachu.js)

- **What have you produced?**

I have produced a picture of Pikachu because it's a well known figure. I used a lot of round shapes, such as the ellipses and points, to recreate Pikachu's most iconic features. To experiment a little, I programmed the mouth to appear once any key is pressed down. I also used hex codes to colour the drawing. 


- **How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?**

Some of the codes were simple enough to understand and play around with, like the points() and ellipses(). Some of them were understandable enough in how they were supposed to work, but actually modifying them as I wanted to proved difficult. I had to make compromises in some places and add extra code in other places, so different functions didn't interfere with each other. 


- **How is the coding process different from, or similar to, reading and writing text?**

It's similar in the way, that reading and writing is pretty straightforward. It wasn't always easy to decipher exactly what was meant though, and in some cases further reading only did little to fully inform me - that's where playing around with the numbers gave a better idea of what a code meant.


- **What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?** 

Coding and programming to me is creativity. The ability to make something silly or pretty, depending on your goal. It's fun to play around with, but can also be used for more serious things and hidden messages in something otherwise simple. The assigned reading, specifically Vee and her notes on literacy, makes me think more on how this is not only a creative outlet, but something more and more people will learn as time goes on. It's useful in many ways, and a stepping stone to more complicated coding programs.


**References used:**
<br>
[Hex codes for colours used](https://www.color-hex.com/color-palette/1043)
<br>
[Picture 1 for inspiration](https://static.posters.cz/image/750/billeder-pokemon-pikachu-i36384.jpg)
<br>
[Picture 2 for inspiration](https://media.wired.com/photos/5f87340d114b38fa1f8339f9/master/w_1600%2Cc_limit/Ideas_Surprised_Pikachu_HD.jpg)

**Bibliography:**

Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48

Annette Vee, "Coding for Everyone and the Legacy of Mass Literacy", Coding Literacty: How Computer Programming Is Changing Writing Cambridge, Mass.: MIT Press, 2017), 43-93. (on blackbord under the Literature folder)
