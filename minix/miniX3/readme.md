**Screenshot:**
<br>
![](https://gitlab.com/celia_w/AP2021/-/raw/master/minix/miniX3/Screenshot_2021-02-17_at_18.14.20.png)

**Program link:**
<br>
[Click here!](https://celia_w.gitlab.io/AP2021/minix/miniX3/)

**Code link:**
<br>
[Click here!](https://gitlab.com/celia_w/AP2021/-/blob/master/miniX2/sketch.js)

In this exercise, I wanted to play around with the throbber as an icon or object that had to reach the other end of the screen. By adding both a moving object and a throbber in it's more original state, the viewer would be able to see an end to their waiting - once the goose reaches the other side, presumably, that's when the waiitng would be over! However, instead of giving the viewer the satisfaction of the wait being over, I added an "error" code, which would display when the goose reached around 3/4th of the way. 
<br>

The code itself made use of the preload() function to load the image of the goose, and the very complex code fro the throbber (which is hard to explain, but it takes the ellipse, draws it 9 times in a rotation and each "old" circle fades due to the background being at 80 opacity or "alpha"). I also had an if-else statement, as well as lots of text() and textAlign() functions. 
<br>

I didn't want to express anything in general, other than how it can feel like you're waiting for forever - and especially how shitty it feels when you wait for a long time, just to see it go wrong and being unable to fix it. For the viewer, the time then moves differently depending on what you focus on - the throbber that goes in cirlces or the goose which seems to have a purpose? Both move, but each has a different relation to time than the other when looking at them - one feels endless, the other has a purpose. When the error screen pops up, it disturbs the viewer and stops time for a second - you were waiting but now you can't do anything!
<br>

Time, in computation, is difficult to explain. In my if-else statement, I'm able to stop and start time in a sense - if x<width/1.5, then something happens. However, x>width/1.5 (my else statement), then times stops for my first part and starts for my second - instead of continously looping and being drawn, I stop it from existing. Additionally, the throbber moves at 8 frames per second. Changing this number gives me the power to slow or speed up time for the computer, as it otherwise runs at 60 frames per second. When I put in an if-else statement, I'm also tampering with time, as it stops or starts running certain parts when a condition is met.
<br>

A throbber is usually an icon for waiting - but in many cases, it also displays that something went wrong and you're nervously left waiting and wondering if it'll fix itself or if you need to do something. It hides what's gone wrong and what's happening while you wait, so you can only stand and wait. I like the throbbers in some games, which show you "loading…", but also explains what's going on - it's loading a picture, a scenery, some voice clips etc. Throbber reimagined as icons or images (such as my goose or other) is also a bit more friendly, as we don't usually look at those and think "oh no", but instead can look at them with some fascination. 


**Reference**: 
<br>
The goose (both image and inspiration) comes from the game: Untitled Goose Game

**Bibliography:**
<br>
Soon Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 71-96
<br>
Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018)
