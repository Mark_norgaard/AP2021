var goose;
var x = 0;

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(8);
  ellipseMode(CENTER);

}

function draw() {
//when goose reaches the end of the canvas, error appears
  if (x<width/1.5){ //goose n' throbber move along...
    background(100, 80);
    drawElements();
    imageMode(CENTER);
    image(goose, x, height/2); //goose "lags" or has a line behind him due to the opacity of the background (to also make the throbber work)
    fill("white");
    textSize(25);
    textAlign(CENTER);
    text("loading...", width/2, height/1.5)
  } else { //oh? error?
     background(17, 115, 170);
     textFont('Arial');
     fill("white");
     textAlign(LEFT);
     textSize(125);
     text(":(", 100, 370)
     textSize(30);
     text("Your PC ran into a problem that it couldn't", 100, 450);
     text("handle, and now it needs to restart.", 100, 500);
     textSize(12);
     text("You can search for the error online: IDK_ASK_GOOGLE", 100, 550);
  }

  x = x + 5;

}

function preload () {
  goose = loadImage ('assets/goose.png');
  }

function drawElements() {
  let num = 9;
  textAlign(CENTER);
  push();
  translate(width/2, height/2);
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));
  fill("white");
  noStroke();
  //the x parameter is the ellipse's distance from the center
  ellipse(35, 0, 22, 22);
  pop();
}
