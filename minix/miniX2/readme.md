**Screenshot:**
<br>
![](Screenshot_2021-02-10_at_22.36.11.png)

**Program link:**
<br>
[Click here!](https://celia_w.gitlab.io/AP2021/minix/miniX2/)

**Code link:**
<br>
[Click here!](https://gitlab.com/celia_w/AP2021/-/blob/master/miniX2/sketch.js)

- **What have I learned?**

My program produces different emoticons, which are produces by a set of letters and characters. Additionally, upon clicking within the canvas, a message saying "don't change" pops up, which is a nod to how emoticons should stay neutral save for any fun colour someone might want to use to personalise an emoticon.

In this program, I was inspired by Multi by David Reinfurt, in which he uses simple characters to create faces. I decided to spin his idea in my own way, by using a few letters and characters that would usually create emoticons I'd use in my daily life. By setting it up similarly to David Reinfurt, it makes up a ton of new emoticon options. The characters and letters chosen were also inspired by a specific shirt (as seen in references). 

For the project itself, I used mousePressed, which was a slight variant to my first keyIsPressed function in my miniX1. Another key detail is, that I went for letters and characters rather than geometric shapes, since I practiced this in my first miniX1. This time around I practiced variables with the letters and the placements of each string of letters. The most troubling part was where to place different things, in which my written code was mostly correct save for the placement of specific parts - before or after function setup() etc. I also struggled with the colours, trying many different combinations to get my desired look - in the end, I looked to Malthe's code from his miniX1, which I had given feedback on, and therefore knew he had the code I needed to get it "right". 

**My work in a wider context:**
<br>
My work is a nod to how emojis came from emoticons. While emojis represent a lot of different things nowadays, it's also easily critised when not representing _everyone_. While I understand the disappointment some may feel, when not being properly represented by emojis, I also think it's a shame that we went from neutral emoticons to emojis that have to fulfill a thousand different demands. With my project, I wanted to highlight the "don't change" part, of how emoticons were used to express _emotions_ without representing people physically. In Roel Roscam Abbing, Peggy Pierrot and Femke Snelting's text "Modifying the Universal", they talk a lot on how demands are bigger - but also how it's not always been done correctly when making new emojis (such as using an index to measure how different skintypes react to ultraviolet rays, but intepreting the colours in differeny ways - making them appear greyer or whiter than they should be). To avoid expanding on the lack of representation, I ultimately decided to "go back to basics", and instead implement colour for each sign to give it some personality and "flair" rather than for it to represent anyone physically. Emoticons are, after all, a neutral representation of mood and emotion. 


**References used:**
<br>
[Malthe Nielsen's miniX1 for inspiration/reference to randomnising the colours](https://gitlab.com/SoelvesterStaniol/ap2021/-/blob/master/miniX1/sketch.js)
<br>
[A shirt that also inspired me](https://teeherivar.com/product/owo-chaotic-neutral-alignment-chart-furry-fandom-meme-rpg-tank-top/)
Multi by David Reinfurt


**Bibliography:**

Soon Winnie & Cox, Geoff, "Variable Geometry", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 51-70

Video/text: Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, "Modifying the Universal." Executing Practices, Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humanities Press, 2018, 35-51. OR Femke Snelting, Modifying the Universal, MedeaTV, 2016
