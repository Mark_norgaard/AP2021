**Screenshot:**
<br>
![](https://gitlab.com/celia_w/AP2021/-/raw/master/minix/miniX5/Screenshot_2021-03-11_at_19.04.07.png)

**Program link:**
<br>
[Click here!](https://celia_w.gitlab.io/AP2021/minix/miniX5/)

**Code link:**
<br>
[Click here!](https://gitlab.com/celia_w/AP2021/-/blob/master/minix/miniX5/sketch.js)



I decided on two rules for me program; the stars would appear at random with random sizes, and addiotnally appear in three different colours of white/pale yellow at some set intervals; between 0.3 and 0.5, higher than 0.5 and lower than 0.3. 

The rules are in place, so that the appearing ecllipes will act lke more natural stars, appearing with different sizes and colours - though only subtle changes. The most important part for me was, that time was slowed by lowering the framerate, so one could sit back and see the canvas fill itself up slowly. I added the change of clearing the stars, so you had the possibility to start over.

The rules' primary role is to create an organic appearance of the stars, which were made with ecllipses. While it's not the most complex code, I liked the idea of creating something familiar for the viewer to see. In relation to the assigned reading, I think I was equally inspired by Langdon's Ant and 10PRINT. With the idea from 10PRINT of random intervals, I assigned a colour to each. Langdon's Ant inspired me to do an organic pattern, and though it's not ruled by "if meeting white cell, turn black and go right", it creates an organic pattern as an "end" result. 

Something that the reading made me think more about, was how "random" would be hard to define - when strictly looking at the screen, it seems the stars are appearing at random, while the code reveals a set of codes to make it appear as such. Simple rules, complex code.

**Bibliography:**
<br>
Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142
<br>
Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, https://10print.org/ (Cambridge, MA: MIT Press, 2012), 119-146.
