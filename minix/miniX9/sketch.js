let birthdays;
let birthdate;
let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
let jsonday;
let dstmonth;
let weather;

function setup() {
  createCanvas(500,400);
  //Following code creates birthdate drop down.
  birthdate = createSelect();
  birthdate.position(150,130);
  birthdate.size(100, 25);
  birthdate.changed(datedropdown);
  for (let i = 1; i <= 31; i++) {
    birthdate.option(i);
  }

  //Folowing code creates month dropdown
  birthmonth = createSelect();
  birthmonth.position(250, 130);
  birthmonth.size(100, 25);
  birthmonth.changed(datedropdown);
  for (let i = 0; i <= months.length; i++){
    birthmonth.option(months[i]);
  }
}

function draw() {
  background(0);

//text on the screen
  push();
  fill(255);
  textSize(13);
  textAlign(CENTER, CENTER);
  text("If isolation kept you in the dark, take this", 250, 35);
  text("chance to relive the weather on your birthday", 250, 55);
  pop();

  // prints number if birthdays in denmark at specific date
  if (birthdays) {
    push();
    fill(255);
    textSize(13);
    textAlign(CENTER, CENTER);
    text("I wonder, were you and the " + birthdays.dataset.value[0] + " other people who celebrated today good or bad?" , 250, 95);
    pop();
  }

  //print temperature value in celcius
  if(weather) {
    push();
    fill(255);
    textAlign(CENTER, CENTER);
    text("So, let's see what history says...", 250, 190);
    push();
    textStyle(ITALIC);
    text(weather.days[0].description, 250, 220);
    text("The temperature was " + weather.days[0].temp + " ℃", 250,250);
    text("But it felt like " + weather.days[0].feelslike + " ℃", 250,270);
    text("Precipitation was at " + weather.days[0].precip, 250,290);
    pop();
    textSize(13);
    text("So... The rest is up to you. Did the weather reflect you?", 250, 330);
    pop();
  }
}



// got data for
function gotData(data) {
  birthdays = data;
  print(data);
}

// got data function for weather json.
function gotData2(data2){
  weather = data2;
  print(data2);
}

// This function handles the dropdown menues and calls the respective API's for information when a date is actively selected.
function datedropdown() {
  //Gets number of birthdays for specific chosen date
  let day = birthdate.value();
  // formats the variable "jsonday" in the form that the API wants it with a 0 in front of single decimal numbers.

  if (day < 10) {
    jsonday = "0"+day;
    print(jsonday)
  } else {
    jsonday = day;
    print(jsonday)
  }

  // Sets month varible to chosen birthmonth.
  let month = birthmonth.value();

  // This takes the month chosen, checks for that month in the list of months,
  // If it is equal to one of the months chosen it checks the index of it and adds 1 so January is 001 and not 000.
  // Then if the month number is above 9 it writes one "0" in front and if its below 10 it writes two i.e. "00".
  for (let i = 0; i < months.length; i++) {
    if (months[i] == month) {
      wthmonth = i+1;
      dstmonth = i+1;
      if (dstmonth < 10) {
        dstmonth = "00"+dstmonth;
        print (dstmonth);
      } else {
        dstmonth = "0"+dstmonth;
        print(dstmonth);
      }
    }
  }

  // gets json file from danmarks statistik for how many birthdays there are presently at dates.
  loadJSON("https://api.statbank.dk/v1/data/folk3/JSONSTAT?valuePresentation=Default&timeOrder=Ascending&FDAG()=D" + jsonday + "&FMAANED="+dstmonth+"&FODAAR=TOT", gotData);

  // gets historical json data for 2020 at specified date.
  loadJSON("https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/Aarhus/2020-" +wthmonth+ "-" + day + "?unitGroup=metric&key=XNAP6CC4EWAGY2VBE797TLG9R", gotData2);
}
