**Screenshot:**
<br>
![](https://gitlab.com/celia_w/AP2021/-/raw/master/minix/miniX7/Screenshot_2021-03-28_at_12.34.07.png)

**Program link:**
<br>
[Click here!](https://celia_w.gitlab.io/AP2021/minix/miniX7/)

**Code link:**
<br>
[Click here!](https://gitlab.com/celia_w/AP2021/-/blob/master/minix/miniX7/sketch.js)


My program has it's roots in Winnie's tofu game (which in turn was inspired / a p5js rework of Tofu Go! by Francis Lam). Due to having a busy week, I decided to rework it, and learn the individual parts better and change them around. The program is about getting serotonin, represented by the ":)" smileys. Your avatar in the game is a hand, which is supposed to catch the serotonin. Additionally, clouds are passing idly by while you concentrate on catching the serotonin.

The serotonin and the clouds are two different objects. The serotonin class has a few properties, being it's speed, size and position on screen. These properties help define the object's movement in the game, which is coming from the bottom and up towards the top, and starting at a random position along the x-axis/with a random x-position. The size and speed is also random, but aren't too big so as to be distracting nor too fast so as to not being able to catch them (hopefully). The clouds have similar properties, with the main different being their movement. I created two cloud classes, one for clouds moving from left to right and one for moving right to left.

The behaviours of the objects are wildly different; the serotonin is a pale yellow ":)" with bold outlines, while the clouds are flattened ellipses with a white colour (with low opacity, defined by the alpha value).

The work doesn't express anything obvious, but it's theme is easy to understand. Serotonin, also known as "the happy chemical", is something we need a bit more of in this pandemic. However, it can be hard to get, as the energy lately isn't what you used to have. The game is simulating this feeling of wanting to catch serotonin, but there's also the chance of missing some because you can't get it fast enough or move fast enough. Similarily, serotonin isn't just something you get out of bed to go get.

The game operates by pressing the left and right arrow keys, to move the hand and catch the serotonin. Using a for-loop and an if and else-if statement, contraints are put in place to register the hand's placement on screen and it's proximity to the serotonin - will you catch it or miss it?


When looking at my program, and keeping in mind the article _The Obscure Objects of Object Orientation_ by Matthew Fuller and Andrew Goffey, objects aren't the same as the real thing. My clouds, as an example, as defined as an ellipse which moves at a set speed between 1 and 2. Additionally, their shape is only changing within certain parametres, but will never resemble the real diversity of clouds. Yet, it's still possibly to get the idea of what they are, by just looking at the program. 

> "a computational object is a _partial_ object: the properties and methods that define it are a necessarily selective and creative abstraction of particular capacities from the thing it models, and which specify the ways that it can be interacted with."

As Fuller and Goffey also point out, objects in computing and coding are _partial_, or half-truths as you could maybe also call it. They will never 100& resemble the real deal, and the object's properties and methods will always be biased in a way, as someone needs to define both - with their own idea of how that object should resemble the real thing.

In a wider context, the problems behind objcets is the bias and the seletctive process you go through, upon creating the object. For me, this especially shows with my creation of the class Serotonin. I had to define how to portray serotonin and chose a smiley rather than something like the molecular structure which would perhaps be a more accurate representation. I also chose yellow, which could portray the sunlight and the happiness one might get from that. But that's _my_ intepretation of serotonin, and it will never be accurate to the scientific one, or every indiviual person's representation of the same thing.

In the same vein, objects in general are a hard thing to define. It's hard to define a person, as we looked at in Winnie Soon and Geoff Cox' book  _Aesthetic Programming_. You have to be selective about what things _define_ a person, as well as avoid bias or insensitive subjects.  


**References:**
<br>
https://www.youtube.com/watch?v=V9NirY55HfU,
Tofu Go! (2018), a game developed and designed by Francis Lam (HK/CN).
<br>
https://gitlab.com/aesthetic-programming/book/-/tree/master/public/p5_SampleCode/ch6_ObjectAbstraction (Winnie's sample code for Tofu Go!)


**Bibliography:**
<br>
Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164
<br>
Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in MatthewFuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017).
