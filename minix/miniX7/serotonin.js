class Serotonin {
  constructor() {
  this.speed = floor(random(2, 4));
  this.size = floor(random(15,35));
  this.pos = new createVector(random(width),height/1.2);
  // this.emoji_size = this.size/1.8;
  }

  move() {  //moving behaviors
    this.pos.y-=this.speed;
  }

  show() {
    push();
    stroke(191, 183, 159);
    strokeWeight(3);
    fill(255, 241, 200);
    textSize(25);
    text(":)", this.pos.x, this.pos.y-15, this.size, this.size);
    pop();
  }
}
