let handSize = {
  w:104,
  h:96
};

let hand;

let handPosX;
let mini_width;

let min_serotonin = 8;
let serotonin = [];

let min_clouds = 3;
let clouds = [];

let min_clouds2 = 3;
let clouds2 = [];

let score =0, lose = 0;

let x;
let y;

function preload(){
  hand = loadImage("assets/hand.png");
}

function setup()  {
  createCanvas(windowWidth, windowHeight);
  handPosX = width/2;
  mini_width = height/2;


}

function draw() {
  background(180, 216, 229);
  fill(255);
  rect(0, height/1.2, width, 1);
  displayScore(); //show scores
  checkSerotoninNum(); //check amount of serotonin on screen
  checkCloudsNum(); //check amount of clouds on screen
  showClouds();
  checkCloudsNum2(); //check amount of clouds on screen
  showClouds2();
  showSerotonin();
  imageMode(CENTER);
  image(hand,handPosX, 50, handSize.w, handSize.h);
  checkEating(); //scoring of serotonin
  checkClouds(); //new clouds appear
  checkClouds2(); //new clouds appear
  checkResult();

  if (keyIsDown(LEFT_ARROW)) {
    handPosX -= 25;
  }
  if (keyIsDown(RIGHT_ARROW)) {
    handPosX += 25;
  }

  if (handPosX > windowWidth) {
    handPosX = windowWidth;
  } else if (handPosX < 0 + handSize.w/2) {
    handPosX = 0 + handSize.w/2;
  }
}

function checkSerotoninNum() {
  if (serotonin.length < min_serotonin) {
    serotonin.push(new Serotonin());
  }
}

function showSerotonin(){
  for (let i = 0; i <serotonin.length; i++) {
    serotonin[i].move();
    serotonin[i].show();
  }
}

function checkCloudsNum() {
  if (clouds.length < min_clouds) {
    clouds.push(new Cloud1());
  }
}

function showClouds(){
  for (let i = 0; i <clouds.length; i++) {
    clouds[i].move();
    clouds[i].show();
  }
}

function checkClouds() {
  for (let i = 0; i <clouds.length; i++){
    if (clouds[i].pos.x < -300) {
      clouds.splice(i,1);
    }
  }
}

function checkCloudsNum2() {
  if (clouds2.length < min_clouds2) {
    clouds2.push(new Cloud2());
  }
}

function showClouds2(){
  for (let i = 0; i <clouds2.length; i++) {
    clouds2[i].move();
    clouds2[i].show();
  }
}

function checkClouds2() {
  for (let i = 0; i <clouds2.length; i++){
    if (clouds2[i].pos.x > width+300) {
      clouds2.splice(i,1);
    }
  }
}

function checkEating() {
  //calculate the distance between each tofu
  for (let i = 0; i < serotonin.length; i++) {
    let d = int(
      dist(handPosX+handSize.w/2-60, handSize.h/2,
        serotonin[i].pos.x, serotonin[i].pos.y)
      );
    if (d < handSize.w/2) { //close enough as if finding good things ig
      score++;
      serotonin.splice(i,1);
    }else if (serotonin[i].pos.y < 10) { //hand missed the stuff
      lose++;
      serotonin.splice(i,1);
    }
  }
}

function displayScore() {
    fill(255);
    textSize(17);
    text("You've gotten "+ score + " doses serotonin today :)", 10, height/1.1);
    text('Oh no, you missed ' + lose + " doses of serotonin :(", 10, height/1.1+20);
    fill(255);
    text('Press left and right arrow keys to get your daily dose of serotonin',
    10, height/1.1+40);
}

function checkResult() {
  if (lose > score && lose > 10) {
    fill(255);
    textSize(26);
    text("You didn't get enough serotonin! :(", width/3, height/1.1);
    noLoop();
  }
}
